var Scene = require("./lib/scene.js");
var Controls = require('./lib/controls.js');
/*
 * Just testing stuff.
 * I promise this isn't my normal code quality.
 */
window.onload = function(){
    console.log("Window loaded");
    var c = document.getElementById("canvas");
    c.width = 1920;
    c.height = 1080;
    // kind of a hack I know
    console.log("Got canvas:",c);
    var s = new Scene(c);
    var frames = [];
    var img = "";
    for(var i = 0; i <= 2; i++){
        img = new Image();
        img.src = "static/frames/noise/" + i + ".jpg";
        frames.push(img);
    }
    var obj = {
        frames: frames,
        _frame: 0,
        update: function(){
            this._frame = this._frame + 1;
            if(this._frame >= this.frames.length){
                this._frame = 0;
            }
        },
        getImage: function(){
            return this.frames[this._frame];
        },
        x: 0,
        y: 0
    };
    s.addChild(obj);
    s.startRender();
    var cntls = new Controls(document.getElementById("canvas-container"),
                             c, s);
    cntls.takeControl();
};

