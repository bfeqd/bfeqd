function Scene(canvas, camera){
    this._canvas = canvas;
    this._heightScaleFactor = 1;
    this._widthScaleFactor = 1;
    this.ctx = canvas.getContext("2d");
    this._children = [];
    if(camera){
        this.camera = camera;
    }
    else{
        this.camera = {
            x: 0,
            y: 0
        };
    }
}

Scene.prototype.setScaleFactor = function(factor){
    this._widthScaleFactor = factor;
    this._heightScaleFactor = factor;
}

Scene.prototype.startRender = function(){
    this._canvas.onclick = this.handleClick.bind(this);
    this._lag = 0;
    // Seed initial value of timestamp
    this._lastTime = performance.now();
    // Start loop
    this.loop(performance.now());
};

Scene.MILISECONDS_PER_TICK = 50;
Scene.prototype.loop = function(time){
    var delta = time - this._lastTime;
    this._lastTime = time;
    this._lag += delta;
    /* 
     * Keep constant update rate by only updating game state
     * in a specific interval. If the user's FPS sucks, this will update the
     * game multiple times a frame
     */
    while(this._lag > Scene.MILISECONDS_PER_TICK){
        this.update();
        this._lag -= Scene.MILISECONDS_PER_TICK;
    }
    /*
     * Pass the delta in so we can interpolate animations, to make the 
     * game look nicer in higher FPS.
     */
    this.render(delta);
    // requestAnimationFrame fires once, so we call it again
    window.requestAnimationFrame(this.loop.bind(this));
};

Scene.prototype.render = function(delta){
    // clear the frame first
    this.ctx.clearRect(0,0,this._canvas.width, this._canvas.height);
    // let the camera update in an interpolated way
    if(this.camera && this.camera.updateInterpolated){
        this.camera.updateInterpolated(delta);
    }
    // Donald Knuth would be mad at me for this
    for(var i = 0, l = this._children.length; i < l; i++){
        var child = this._children[i];
        var img = child.getImage();
        this.ctx.drawImage(img,
                           child.x - this.camera.x,
                           child.y - this.camera.y,
			               img.width * this._widthScaleFactor, 
                           img.height * this._heightScaleFactor)
    }
}
Scene.prototype.update = function(){
    if(this.camera && this.camera.update){
        this.camera.update();
    }
    // Donald Knuth would be mad at me for not using .forEach here
    for(var i = 0, l = this._children.length; i < l; i++){
        this._children[i].update();
    }
};

/**
 * Add a child to this scene
 * @param {SceneNode} node - the node to add to this scene
 */
Scene.prototype.addChild = function(child){
    console.log("Adding child:",child);
    this._children.push(child);
};
Scene.prototype.handleClick = function(event){
    console.log("Clicked!");
};

module.exports = Scene
