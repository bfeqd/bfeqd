function Controls(container, canvas, scene){
    this.scene = scene;
    this.canvas = canvas;
    this.container = container;
}

Controls.prototype.takeControl = function(){
    document.getElementById("fullscreen").onclick = this.fullscreen.bind(this);
    document.getElementById("resolution").onchange = this.setres.bind(this);
}

Controls.prototype.fullscreen = function(event){
    var c = this.container;
    if(c.requestFullscreen){
        c.requestFullscreen();
    } else if (c.msRequestFullscreen){
        c.msRequestFullscreen();
    } else if (c.mozRequestFullScreen){
        c.mozRequestFullScreen();
    } else if (c.webkitRequestFullscreen){
        c.webkitRequestFullscreen();
    } else {
        alert("Fullscreen not supported, sorry.");
    }

}

Controls.prototype.setres = function(e){
    factor = parseFloat(e.target.options[e.target.selectedIndex].value);
    console.log(factor);
    console.log(this.canvas);
    this.canvas.width = 1920 * factor;
    this.canvas.height = 1080 * factor;
    this.scene.setScaleFactor(factor);
}
module.exports = Controls
