# Setup (Development)
1. Sync everything! Be sure to also sync git-annex
1. Install browserify (`npm install -g browserify`)
1. Install watchify (`npm install -g watchify`)
1. Use `rake watcherify` to live update changes.
